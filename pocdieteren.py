# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 16:04:18 2021

@author: SO20163710 Sofie De Souter
"""
##############################################################################
## Update local path where data is stored + output path for training locally
##############################################################################
# SDS
# inputpath = 'C:/Users/SO20163710/OneDrive/4C/TellMi/DIeteren/data/'
# outputpath = 'C:/Users/SO20163710/Documents/tellmi-absm-sagemaker/input/data/train'
# JS
inputpath = 'C:/Users/JO20169953/Desktop/TellMi - DIeteren/data/'
outputpath = 'C:/Users/JO20169953/Documents/tellmi-absm-sagemaker/input/data/train'

##############################################################################
## Modules
##############################################################################
import os
import pandas as pd
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

##############################################################################
## Functions
##############################################################################
def countinglabels(dataset,label_type,max_labels=4):
    labels = {}
    for label in range(max_labels):
        labels[f'{label}'] = dataset[(label_type+str(label+1))].value_counts().to_frame()
    output = pd.concat(labels.values(),axis=1).fillna(0)
    output['Total Count'] = output.sum(axis=1)
    print("Out of " + str(output.shape[0]) + " unique labels for " + str(label_type) + ", " + str((output['Total Count']>250).sum()) + " labels were assigned to at least 250 text records.") 
    return output['Total Count']


def countinglabelsvisual(dataset,label_type,title,palette="husl",max_labels=4):
    labels = {}
    for label in range(max_labels):
        labels[f'{label}'] = dataset[(label_type+str(label+1))].value_counts().to_frame()
    output = pd.concat(labels.values(),axis=1).fillna(0)
    output['Total Count'] = output.sum(axis=1)
    print("Out of " + str(output.shape[0]) + " unique labels for " + str(label_type) + ", " + str((output['Total Count']>250).sum()) + " labels were assigned to at least 250 text records.")
    sns.barplot(y=output.index,x=output['Total Count'])
    sns.set_palette(palette)
    #https://seaborn.pydata.org/tutorial/color_palettes.html
    sns.set_context("paper", font_scale=2.0) 
    plt.axvline(250, linestyle = "--")   
    plt.title(str(title) + " " + str(label_type),fontsize=20)
    #plt.set_xlabel("xlabel",fontsize=30)
    #plt.set_ylabel("ylabel",fontsize=20)
    plt.show()

def extracttopicsubtopic(dataset,maxlabels=4):
    dataset_pertopic= pd.DataFrame(columns=['Text','Topic','Subtopic'])
    for i, row in dataset.iterrows():
        for j in range(max_labels):
            if isinstance(row['Topic'+str(j+1)],str):
        #print(row['Topic1'])
                dataset_pertopic = dataset_pertopic.append({'Text': row['Text'], 'Topic': row['Topic' +str(j+1)], 'Subtopic': row['Subtopic'+str(j+1)]}, ignore_index=True)
    return dataset_pertopic   
 
def countcombinations(dataset,maxlabelsminus1 = 3):
    topics = extracttopicsubtopic(dataset)['Topic'].unique()
    topic_combinations = pd.DataFrame(columns=topics.tolist(),index=topics.tolist())
    topic_combinations = topic_combinations.fillna(0)
    for i, row in dataset.iterrows():
        for j in range(maxlabelsminus1):
            if (isinstance(row['Topic'+str(j+1)],str) & isinstance(row['Topic'+str(j+2)],str)):
                #print(str(type(row['Topic'+str(j+1)])) + ' and ' + row['Topic'+str(j+2)])
                topic_combinations.loc[row['Topic'+str(j+1)],row['Topic'+str(j+2)]] += 1
                topic_combinations.loc[row['Topic'+str(j+2)],row['Topic'+str(j+1)]] += 1
    return topic_combinations

def drop_fullrecords_containing_smalllabels(data, threshold, maxlabels=4):
    initial_length = len(data) 
    valuecounts = countinglabels(data,'Topic').to_frame()
    topics_to_drop = valuecounts[valuecounts['Total Count']<threshold]
    for h in topics_to_drop.index:  
        for j in range(maxlabels):
            for i, row in data.iterrows():
                if row['Topic'+str(j+1)] == h:
                    data = data.drop(i)
    print(str(initial_length-len(data)) + ' records dropped') 
    print(str(len(data)) + ' records left')
    return data

##############################################################################
## Data Import
##############################################################################
# in case dates need to be used: note that I have corrected date format in Excel before importing
# get data file names
os.chdir(inputpath)
filenames = os.listdir(inputpath)

# import all files while adding brand and year (filename structure BRAND_YEAR_...)
d = {}
for file in filenames:
    d[f'{file}'] = pd.read_excel(file,sheet_name=0)
    d[f'{file}']['Brand'] = file.lstrip().split('_')[0]
    d[f'{file}']['Year'] = file.split('_')[1].lstrip()

##############################################################################
## Cleanse column names + create basetable
##############################################################################
# COLUMN NAMES
# print column names of each file
for i in range(len(filenames)):
    print(d[filenames[i]].columns)

# conclusion column name swaps
# 1. LANGUAGE = Taal
# 2. TextSplit = Commentaar Algemene tevredenheid
# 3. NewSubtopic1 = Subtopic 1 (1 to 4)
# 4. NewTheme1 = Theme 1 (1 to 4)
# 5. NewTopic1 = Topic 1 (1 to 4)
# extra columns for some files Actionable? + Follow-up?
#test"
column_renaming = {"LANGUAGE":"Taal","TextSplit":"Commentaar Algemene tevredenheid","NewSubtopic1":"Subtopic 1","NewSubtopic2":"Subtopic 2","NewSubtopic3":"Subtopic 3","NewSubtopic4":"Subtopic 4","NewTheme1":"Theme 1","NewTheme2":"Theme 2","NewTheme3":"Theme 3","NewTheme4":"Theme 4","NewTopic1":"Topic 1","NewTopic2":"Topic 2","NewTopic3":"Topic 3","NewTopic4":"Topic 4"}
#d[filenames[0]] = d[filenames[0]].rename(columns=column_renaming)
for i in range(len(filenames)):
    d[filenames[i]] = d[filenames[i]].rename(columns=column_renaming)
 
# same column names in 2 first files
d[filenames[0]].columns.intersection(d[filenames[1]].columns)
# column names only available in first file
d[filenames[0]].columns.difference(d[filenames[1]].columns)
# column names only available in second file
d[filenames[1]].columns.difference(d[filenames[0]].columns)

# MERGE DATA

# create 1 basetable with all data
basetable = pd.DataFrame()
for i in range(len(filenames)):
    basetable = pd.concat([basetable,d[filenames[i]]], ignore_index = True)

# validation check: are the column names set as defined in the app?
data_columns_app = basetable.copy()

# as next steps has shown column Text is missing: ###hardcoded fix
data_columns_app.rename(columns={'Commentaar Algemene tevredenheid': 'Text','Taal':'Language'}, inplace=True)
print('**** Column "Commentaar Algemene tevredenheid" was renamed "Text" and "Taal" was renamed "Language" to match the column names in the app')
# remove spaces in column names
data_columns_app = data_columns_app.rename(columns=lambda x: x.replace(" ",""))
print('*** Spaces in column names were removed to match columns names in the app. E.g. "Topic 1" was renamed "Topic1"')
data_columns_app.columns

# check the renaming
nb_changes  = 0
columns_app = ['Text', 'Language', 'Topic1', 'Sentiment1', 'Topic2', 'Sentiment2', 'Topic3', 'Sentiment3', 'Topic4', 'Sentiment4']
columns_ori = data_columns_app.columns
for i in range(0, len(columns_app)):
    if columns_app[i] not in columns_ori:
        if columns_app[i].lower() in columns_ori.str.lower():
            print('CAUTION: column name %s seems to be missing a capital letter' %columns_app[i].lower())
            # option #1:  capitalize
            data_columns_app.rename(columns={columns_app[i].lower(): columns_app[i]}, inplace=True)
            nb_changes += 1
        else:
            print('CAUTION: column name %s seems to be missing. Investigate this first before continuing!' %columns_app[i])
            sys.exit()

if nb_changes > 0:
    print('**** After %d change(s), the new column names are: ' %nb_changes, data_columns_app.columns)

basetable = data_columns_app.copy()
basetable.columns

##############################################################################
## Check data quality of language
##############################################################################
# TAAL
plt.style.use('ggplot')
basetable['Language'].hist()
#plt.hist(basetable['Language'])
#plt.show()
# change language values to lower case
basetable['Language'] = basetable['Language'].str.lower()
# check changes
basetable['Language'].hist()
# correction 26/4/2021: remove German records as we can only train Dutch, French, English and Spanish atm
basetable = basetable[basetable['Language']!='de']

##############################################################################
## Check data quality of themes
##############################################################################
# THEMES
# check values
max_labels = 4
for i in range(0,max_labels):
    basetable['Theme'+str(i+1)].hist()
# cleanse ###hardcoded fix
for i in range(0,max_labels):
    basetable['Theme'+str(i+1)] = basetable['Theme'+str(i+1)].replace(['sales','Salesman'],'Sales')

# still 1 odd value in Theme4: 'Positive' -> check reason and correct
theme_typo_index = basetable['Theme4'] == 'Positive'
print(basetable.loc[theme_typo_index,['Theme4', 'Topic4', 'Subtopic4', 'Sentiment4']])
# only theme 4 is filled out by accident -> check level 3
print(basetable.loc[theme_typo_index,['Theme3', 'Topic3', 'Subtopic3']])
print(basetable.loc[theme_typo_index,['Sentiment3']])
# for the particular record: shift subtopic & sentiment 3 to the left
basetable.loc[theme_typo_index,"Topic3"] = basetable.loc[theme_typo_index,"Subtopic3"]
basetable.loc[theme_typo_index,"Subtopic3"] = basetable.loc[theme_typo_index,"Sentiment3"]
basetable.loc[theme_typo_index,"Sentiment3"] = basetable.loc[theme_typo_index,"Theme4"]
basetable.loc[theme_typo_index,"Theme4"] = float("NaN")
# review histogram of theme 4
basetable['Theme4'].hist()

##############################################################################
## Check data quality of topics
##############################################################################
for i in range(0,max_labels):
    sns.countplot(y=basetable['Topic'+str(i+1)])
    plt.show()
# data quality ok
# 1 record has Behavior/friendliness as topic1 -> shift because this is the subtopic under 'Salesman'
shift_topic_index = basetable['Topic1']=='Behavior/friendliness'
basetable.loc[shift_topic_index,'Subtopic1'] = basetable.loc[shift_topic_index,'Topic1']
basetable.loc[shift_topic_index,'Topic1'] = 'Salesman'
# 1 record has Behavior/friendliness as topic1 -> shift because this is the subtopic under 'Salesman'
shift_topic_index = basetable['Topic2']=='Competence/advice'
basetable.loc[shift_topic_index,'Subtopic2'] = basetable.loc[shift_topic_index,'Topic2']
basetable.loc[shift_topic_index,'Topic2'] = 'Salesman'

##############################################################################
## Check data quality of subtopics
##################################################g############################
for i in range(0,max_labels):
    print("Subtopic %s:" %(i+1))
    print(basetable['Subtopic'+str(i+1)].value_counts())
# data quality ok

##############################################################################
## Check data quality of sentiment
##############################################################################
for i in range(0,max_labels):
    sns.countplot(y=basetable['Sentiment'+str(i+1)])
    plt.show()
    #print(basetable['Sentiment'+str(i+1)].value_counts())

# Sentiment 1 contains 1 record 'sales' and 1 record 'General'
print(basetable[basetable['Sentiment1']=='sales']['Text'])
# Pos
print(basetable[basetable['Sentiment1']=='General']['Text'])
# Neutral
basetable.loc[basetable['Sentiment1']=='sales','Sentiment1'] = 'Positive'
basetable.loc[basetable['Sentiment1']=='General','Sentiment1'] = 'Neutral'

# Correction 26/04/2021: decided to drop neutral records

basetable['NeutralCount'] = 0  
for i, row in basetable.iterrows():
    for j in range(4):
        if (row['Sentiment'+str(j+1)]=='Neutral'):
            basetable.loc[i,'NeutralCount'] += 1

for i, row in basetable.iterrows():
    if basetable.loc[i,'NeutralCount'] > 0:
        basetable = basetable.drop(i)
        #print('Record '+ str(i) + ' dropped.')
            

##############################################################################
## Validation check: do all records have language identified?
##############################################################################
basetable['Language'].fillna('No language defined').value_counts()
# 13 records have no language defined -> manual
no_language = basetable[basetable['Language'].isna()]
print(no_language)
# all in French
basetable['Language'] = basetable['Language'].fillna('fr')
# check result
basetable['Language'].fillna('No language defined').value_counts()

##############################################################h################
## Validation check: is there a sentiment for every topic and the other 
#                   way around?
##############################################################################
columns_topic     = ['Topic1', 'Topic2', 'Topic3', 'Topic4']
columns_sentiment = ['Sentiment1', 'Sentiment2', 'Sentiment3', 'Sentiment4']


data_label_nomismatch = basetable.copy()

for i in range(0, len(columns_topic)):
    #data_label_nomismatch.loc[(data_label[columns_topic[i]].notnull() != data_label[columns_sentiment[i]].notnull()), column_topic[i]]     = np.nan
    #data_label_nomismatch.loc[(data_label[columns_topic[i]].notnull() != data_label[columns_sentiment[i]].notnull()), columns_sentiment[i]] = np.nan
    # determine the mismatches for each topic and sentiment column
    l_mismatch = np.where(data_label_nomismatch[columns_topic[i]].notnull() != data_label_nomismatch[columns_sentiment[i]].notnull())[0]
    if len(l_mismatch) > 0:
        print('CAUTION: for %s text document(s) there is a %s but no %s (or the other way around) defined'
              %(len(l_mismatch), columns_topic[i], columns_sentiment[i]))
        data_label_nomismatch[columns_topic[i]].iloc[l_mismatch]     = np.nan #gives SettingWithCopyWarning
        data_label_nomismatch[columns_sentiment[i]].iloc[l_mismatch] = np.nan #gives SettingWithCopyWarning

# remove all rows without a decent label (can have a label in topic 2 but not in topic 1)
data_label_nomismatch.dropna(how='all', subset = columns_topic, inplace = True)
print(str(basetable.shape[0]-data_label_nomismatch.shape[0]) + " records dropped")

##############################################################################
## Validation check: do all records have topic & subtopic?
##############################################################################
# columns_subtopic = ['Subtopic1', 'Subtopic2', 'Subtopic3', 'Subtopic4']
# for i in range(0, len(columns_topic)):
#     #data_label_nomismatch.loc[(data_label[columns_topic[i]].notnull() != data_label[columns_sentiment[i]].notnull()), column_topic[i]]     = np.nan
#     #data_label_nomismatch.loc[(data_label[columns_topic[i]].notnull() != data_label[columns_sentiment[i]].notnull()), columns_sentiment[i]] = np.nan
#     # determine the mismatches for each topic and sentiment column
#     l_mismatch = np.where(data_label_nomismatch[columns_topic[i]].notnull() != data_label_nomismatch[columns_subtopic[i]].notnull())[0]
#     if len(l_mismatch) > 0:
#         print('CAUTION: for %s text document(s) there is a %s but no %s (or the other way around) defined'
#               %(len(l_mismatch), columns_topic[i], columns_subtopic[i]))
#         # option #1: replace by a nan
#         data_label_nomismatch[columns_topic[i]].iloc[l_mismatch]     = np.nan #gives SettingWithCopyWarning
#         data_label_nomismatch[columns_subtopic[i]].iloc[l_mismatch] = np.nan #gives SettingWithCopyWarning
        # option #2: fill in missing topic or sentiment manually
#CAUTION: for 1721 text document(s) there is a Topic1 but no Subtopic1 (or the other way around) defined
#CAUTION: for 656 text document(s) there is a Topic2 but no Subtopic2 (or the other way around) defined
#CAUTION: for 239 text document(s) there is a Topic3 but no Subtopic3 (or the other way around) defined
#CAUTION: for 69 text document(s) there is a Topic4 but no Subtopic4 (or the other way around) defined

# ??actions to take?? We won't take action as we will not build the model on subtopic level (only topic level)






##############################################################################
## Split Aftersales and Sales
##############################################################################
# correction 26/4/2021: remove records where text is null
data_label_nomismatch = data_label_nomismatch[data_label_nomismatch['Text'].notnull()]

# for every record, count the labels for each theme
themes = ["Sales","Aftersales"]
data_label_nomismatch['Count'+str(themes[0])] = 0
data_label_nomismatch['Count'+str(themes[1])] = 0
for i, row in data_label_nomismatch.iterrows():
    for j in range(max_labels):
        if row['Theme'+str(j+1)] == themes[0]:
            data_label_nomismatch.loc[i,'Count'+str(themes[0])] += 1
        elif row['Theme'+str(j+1)] == themes[1]:
            data_label_nomismatch.loc[i,'Count'+str(themes[1])] += 1
#print(data_label_nomismatch[['Theme1','Theme2','Theme3','Theme4','CountSales','CountAftersales']].head())

# for every record, decide which model to assign to
data_label_nomismatch['Assign'] = 'initial'
for i, row in data_label_nomismatch.iterrows():
    if row['Count'+str(themes[0])] > 0 and row['Count'+str(themes[1])] > 0:
        data_label_nomismatch.loc[i,'Assign'] = 'Mix'
    elif row['Count'+str(themes[0])] > 0 and row['Count'+str(themes[1])] == 0:
        data_label_nomismatch.loc[i,'Assign'] = str(themes[0])
    elif row['Count'+str(themes[0])] == 0 and row['Count'+str(themes[1])] > 0:
        data_label_nomismatch.loc[i,'Assign'] = str(themes[1])
print(data_label_nomismatch['Assign'].value_counts())
# 75 records contain a mix of themes in the labels

sales = data_label_nomismatch[data_label_nomismatch['Assign']==themes[0]]
aftersales = data_label_nomismatch[data_label_nomismatch['Assign']==themes[1]]

##############################################################################
## Explore Sales & Aftersales dataset: check records per topic
##############################################################################
# #SALES
countinglabels(dataset = sales, label_type = 'Topic')
countinglabelsvisual(dataset=sales, label_type="Topic", title="Sales",palette="hls")
# countinglabels(dataset = sales, label_type = 'Sentiment')
# countinglabelsvisual(dataset=sales, label_type="Sentiment", title="Sales",palette="Set2")
# countinglabels(dataset = sales, label_type = 'Subtopic')
# #AFTERSALES
countinglabels(dataset = aftersales, label_type = 'Topic')
countinglabelsvisual(dataset=aftersales, label_type="Topic", title="Aftersales",palette="hls")
# countinglabels(dataset = aftersales, label_type = 'Sentiment')
# countinglabelsvisual(dataset=aftersales, label_type="Sentiment", title="Aftersales",palette="Set2")
# countinglabels(dataset = aftersales, label_type = 'Subtopic')


##############################################################################
## REVIEW RELABELLING AFTER TURNING OFF VALIDATION CHECK TOPIC/SUBTOPIC
##############################################################################
# counting subtopics --> analysis in Excel
# #SALES
#path = 'C:/Users/JO20169953/Desktop/TellMi - DIeteren/Test counting subtopics'
#os.chdir(path)
#extracttopicsubtopic(dataset = sales, maxlabels=4).to_excel('sales_data_count.xlsx')
# #AFTERSALES
#extracttopicsubtopic(dataset = aftersales, maxlabels=4).to_excel('aftersales_data_count.xlsx')

# for sales, manually relabel Salesman topics with no subtopics (60) and Car_delivery topics with no subtopic (236) --> in Excel
#path = 'C:/Users/JO20169953/Desktop/TellMi - DIeteren/Relabelling Salesman'
#os.chdir(path)
#sales.to_excel('sales_for_relabelling.xlsx')
#also check for aftersales
#aftersales.to_excel('aftersales_for_relabelling.xlsx')
#sales = pd.read_excel(r'C:\Users\JO20169953\Desktop\TellMi - DIeteren\Relabelling Salesman\sales_for_relabelling.xlsx')

##############################################################################
## Update classification of labels Sales dataset (Confirmed by D'Ieteren on 22/4/2021)
##############################################################################
def replacingtopics_sales(dataset,max_labels=4):
    for i, row in dataset.iterrows():
        for label in range(max_labels):
            if row[('Subtopic'+str(label+1))] == 'Duration of delivery':
                dataset.loc[i,('Topic'+str(label+1))] = 'Duration of car delivery'
            if row[('Subtopic'+str(label+1))] == 'Duration of explanations':
                dataset.loc[i,('Topic'+str(label+1))] = 'Duration of car delivery'
            if row[('Subtopic'+str(label+1))] == 'Information at delivery':
                dataset.loc[i,('Topic'+str(label+1))] = 'Quality of explanations at car delivery'
            if row[('Subtopic'+str(label+1))] == 'Information/updates about delivery date':
                dataset.loc[i,('Topic'+str(label+1))] = 'Service related to car delivery'
            if row[('Subtopic'+str(label+1))] == 'Process/admin car delivery':
                dataset.loc[i,('Topic'+str(label+1))] = 'Service related to car delivery'
            if row[('Subtopic'+str(label+1))] == 'Service related to car delivery':
                dataset.loc[i,('Topic'+str(label+1))] = 'Service related to car delivery'
            if row[('Subtopic'+str(label+1))] == 'Quality of explanations':
                dataset.loc[i,('Topic'+str(label+1))] = 'Quality of explanations at car delivery'
            if row[('Subtopic'+str(label+1))] == 'Waiting period / announced delivery date':
                dataset.loc[i,('Topic'+str(label+1))] = 'Duration of car delivery'
            #if row[('Subtopic'+str(label+1))] == 'Car availability / production date':
             #   dataset.loc[i,('Topic'+str(label+1))] = 'Service related to car delivery'
            #if row[('Subtopic'+str(label+1))] == 'Electric/hybrid car':
             #   dataset.loc[i,('Topic'+str(label+1))] = 'Service related to car delivery'
            if row[('Topic'+str(label+1))] == 'Salesman':
                dataset.loc[i,('Topic'+str(label+1))] = ('Salesman ' + str(row[('Subtopic'+str(label+1))]))
    return dataset                           
                
sales = replacingtopics_sales(sales)
countinglabelsvisual(dataset=sales, label_type="Topic", title="Sales",palette="hls")
countinglabels(sales,'Topic').to_frame()

##############################################################################
## Sales: delete all records where at least 1 of the categories with less than 250 labels are present
##############################################################################
# Business check: we drop Other_sales, Service related to car delivery, Price_sales, Test_drive, Showroom, Take_over and Financing_leasing
countinglabels(sales,'Topic').to_frame()
sales_small_topics_dropped = drop_fullrecords_containing_smalllabels(sales, 250)
countinglabels(sales_small_topics_dropped,'Topic').to_frame()

countinglabelsvisual(sales_small_topics_dropped, 'Topic', 'Sales Validated')

############################################################################
## Update classification of labels Aftersales dataset (Confirmed by D'Ieteren on 22/4/2021)
##############################################################################
def replacingtopics_aftersales(dataset,max_labels=4):
    for i, row in dataset.iterrows():
        for label in range(max_labels):
            if row[('Topic'+str(label+1))] == 'Employee_receptionist':
                dataset.loc[i,('Topic'+str(label+1))] = ('Employee_receptionist ' + str(row[('Subtopic'+str(label+1))]))
            if row[('Subtopic'+str(label+1))] == 'Cleanliness of car':
                dataset.loc[i,('Topic'+str(label+1))] = 'Quality of service'
            if row[('Subtopic'+str(label+1))] == 'Did what was agreed':
                dataset.loc[i,('Topic'+str(label+1))] = 'Did what was agreed'
            if row[('Subtopic'+str(label+1))] == 'New issue':
                dataset.loc[i,('Topic'+str(label+1))] = 'Quality of service'
            if row[('Subtopic'+str(label+1))] == 'Quality of work/Problem solved':
                dataset.loc[i,('Topic'+str(label+1))] = 'Quality of service'
#            if row[('Subtopic'+str(label+1))] == 'Confirmation':
#                dataset.loc[i,('Topic'+str(label+1))] = 'Confirmation, duration and respecting the appointment'
#            if row[('Subtopic'+str(label+1))] == 'Duration until appointment':
#                dataset.loc[i,('Topic'+str(label+1))] = 'Confirmation, duration and respecting the appointment'
#            if row[('Subtopic'+str(label+1))] == 'Respecting appointment':
#                dataset.loc[i,('Topic'+str(label+1))] = 'Confirmation, duration and respecting the appointment'
#            if row[('Subtopic'+str(label+1))] == 'Online appointment':
#                dataset.loc[i,('Topic'+str(label+1))] = 'Online and reachability'
#            if row[('Subtopic'+str(label+1))] == 'Reachability':
#                dataset.loc[i,('Topic'+str(label+1))] = 'Online and reachability'
           
    return dataset            
                             
aftersales = replacingtopics_aftersales(aftersales)
countinglabelsvisual(dataset=aftersales, label_type="Topic", title="Aftersales",palette="hls")
countinglabels(aftersales,'Topic').to_frame()

##############################################################################
## Aftersales: delete all records where at least 1 of the categories with less than 250 labels are present
##############################################################################
# Business check: we drop Other_aftersales, Price_aftersales, Duration, Replacement_mobility, Drop_off_pick_up, Warranty_weCare_recall, Customer_support, Car_experience and Website_app
# Take 249 so that employee_receptionist Reactivity/follow-up... is also included
countinglabels(aftersales,'Topic').to_frame()
aftersales_small_topics_dropped = drop_fullrecords_containing_smalllabels(aftersales, 249)
countinglabels(aftersales_small_topics_dropped,'Topic').to_frame()

countinglabelsvisual(aftersales_small_topics_dropped, 'Topic', 'Aftersales Validated')


# BELOW = NO LONGER TRUE
# As a result, Infrastructure and Employee_receptionist Reactivity/follow-up/reac.. has less than 250 records
#aftersales_small_topics_dropped_2 = drop_fullrecords_containing_smalllabels(aftersales_small_topics_dropped, 232)
#countinglabels(aftersales_small_topics_dropped_2,'Topic').to_frame()

#countinglabelsvisual(aftersales_small_topics_dropped_2, 'Topic', 'Aftersales Validated')

# FINAL DATA CLEANING STEP: delete doubles (for 1 text 2 times same topic - subtopic)
## note that this is not only possible for the subtopics of which we changed the label, also in others = bad labelling
#path = 'C:/Users/JO20169953/Desktop/TellMi - DIeteren/Final cleaning step'
#os.chdir(path)
#sales_small_topics_dropped.to_excel('sales_for_final_cleaning.xlsx')
#aftersales_small_topics_dropped.to_excel('aftersales_for_final_cleaning.xlsx')
# READ IN THE CORRECT FILES HERE
sales_clean = pd.read_excel(r'C:\Users\JO20169953\Desktop\TellMi - DIeteren\Final cleaning step\sales_for_final_cleaning.xlsx')
aftersales_clean = pd.read_excel(r'C:\Users\JO20169953\Desktop\TellMi - DIeteren\Final cleaning step\aftersales_for_final_cleaning.xlsx')

## final division
countinglabels(sales_clean,'Topic').to_frame()
countinglabelsvisual(sales_clean, 'Topic', 'Sales - final division')

countinglabels(aftersales_clean,'Topic').to_frame()
countinglabelsvisual(aftersales_clean, 'Topic', 'Aftersales - final division')



##############################################################################
## Sales & Aftersales: export cleaned data
##############################################################################
# Save Sales OR Aftersales dataset in input folder in order to train model
os.chdir(outputpath)
# Be careful saving training data: only sales OR aftersales (overwriting of training_data.csv)
columns_tellmi = ['Text','Language','Topic1','Sentiment1','Topic2','Sentiment2','Topic3','Sentiment3','Topic4','Sentiment4']
#sales_clean[columns_tellmi].to_csv('training_data.csv', index=False)
aftersales_clean[columns_tellmi].to_csv('Aftersales_training_data.csv', index=False)



